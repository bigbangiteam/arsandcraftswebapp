$(function() {

    $("input,textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            // Prevent spam click and default submit behaviour
            $("#btnSubmit").attr("disabled", true);
            event.preventDefault();
            
            // get values from FORM
            var name = $("input#name").val();
            var email = $("input#email").val();
            var phone = $("input#phone").val();
            var message = $("textarea#message").val();
            var firstName = name; // For Success/Failure Message
            // Check for white space in name for Success/Fail message
            if (firstName.indexOf(' ') >= 0) {
                firstName = name.split(' ').slice(0, -1).join(' ');
            }
            
            
            $.ajax({
                type: 'POST',
				  url: 'https://mandrillapp.com/api/1.0/messages/send.json',
				  data: {
					'key': 'cggvNv3rlFpIw82HtgnAig',
					'message': {
					  'from_email': email,
					  'to': [
						  {
							'email': 'basturk90.un@hotmail.it',
							'name': 'name',
							'type': 'to'
						  }
						],
					  'autotext': 'true',
					  'subject': 'ArsAndCrafts richiesta contatto: '+name,
					  'html': 'Hai ricevuto un messaggio dal form del sito www.arsandcrafts.com.<br/><br/>Questi i dettagli:<br/><br/>Nome: '+name+'<br/><br/>Email: '+email+'<br/><br/>Telefono: '+phone+'<br/><br/>Messaggio:<br/>'+message
					}
				  },
                cache: false,
                success: function() {
                    // Enable button & show success message
                    $("#btnSubmit").attr("disabled", false);
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-success')
                        .append("<strong>Il tuo messaggio &egrave stato inviato. </strong>");
                    $('#success > .alert-success')
                        .append('</div>');

                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append("<strong>Mi dispiace " + firstName + ", sembra che il mio mail server non risponda. Per favore riprova pi&ugrave tardi!");
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});

// When clicking on Full hide fail/success boxes
$('#name').focus(function() {
    $('#success').html('');
});
