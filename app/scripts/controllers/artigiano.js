/**
 * Created by daniele on 26/07/15.
 */
'use strict';

angular.module('arsandcraftsApp')

  .controller('ArtigianoController', ['$scope', 'ArtigianoSrvc', 'FileUploader', '$location',    function ($scope, ArtigianoSrvc, FileUploader, $location) {

    $scope.prodottoList = [];
    $scope.fotoList     = [];
    $scope.vista        = 'prodotto';
    $scope.checkModel   = {};
    var idProdotto = 0;

    //ArtigianoSrvc.getArtigiano(1);

    ArtigianoSrvc.getAllProdotto().then(
      function(response){
		console.log("getAllProdotto response: "+response.data.data.prodottoList);
        var prodottoList = response.data.data.prodottoList;
        $scope.prodottoList.length = 0;

        angular.forEach(prodottoList, function(prodotto){			
		console.log("prodotto.fotoList[0].idFoto: "+response.data.data.prodottoList);
          if(prodotto.fotoList[0].idFoto) {
            prodotto.immagine = ArtigianoSrvc.getDataImmagine(prodotto.fotoList[0].idFoto);
            $scope.prodottoList.push(prodotto);
          }
        });
      },
      function(response){

      },
      function(response){

      }
    );

    $scope.dettaglioProdotto = function(prodottoId){
		idProdotto=prodottoId;
      createUploader(prodottoId);

      ArtigianoSrvc.getProdottoById(prodottoId).then(
        function(response){

          var fotoList = response.data.data.fotoList;
          $scope.descrizioneProdotto = response.data.data;
          delete $scope.descrizioneProdotto.fotoList;

          $scope.fotoList.length = 0;

          angular.forEach(fotoList, function(foto){

			foto.mediaFeedback=foto.mediaFeedback< 1.5?'Inguardabile!':(foto.mediaFeedback<2.5?'Decente..':(foto.mediaFeedback<3.5?'Buona':(foto.mediaFeedback<4.5?'Bella..':('Spettacolo!'))));
              foto.immagine = ArtigianoSrvc.getDataImmagine(foto.idFoto);
              $scope.fotoList.push(foto);

          });
        },
        function(response){

        },
        function(response){

        }
      );

      $scope.vista = 'dettaglio';
      
    };

    $scope.elencoProdotto = function(){
      $scope.fotoList.length = 0;
      $scope.descrizioneProdotto = null;
    };

    $scope.showCaption = function(){
      $scope.captionActive = true;
    };

    $scope.hideCaption = function(){
      $scope.captionActive = false;
    };
    
    $scope.backProducts = function(){
		console.log("back products");
		
		$scope.vista        = 'prodotto';
	}
	
    $scope.elimina = function(id) {
		 ArtigianoSrvc.deleteProdottoById(id).then(
        function(response){
			console.log(response);
			$location.path('/home');
        },
        function(response){
			console.log(response);
        },
        function(response){
			console.log(response);
        }
      );
	}

    var createUploader = function(id) {
		console.log('createuploader('+id+')');
      var uploader = $scope.uploader = new FileUploader({
        url: 'rest/artisan/file/foto/' + id
      });

      uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
          return this.queue.length < 10;
        }
      });

      // CALLBACKS

      uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
      };
      uploader.onAfterAddingFile = function (fileItem) {
        console.info('onAfterAddingFile', fileItem);
      };
      uploader.onAfterAddingAll = function (addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
      };
      uploader.onBeforeUploadItem = function (item) {
        console.info('onBeforeUploadItem', item);
      };
      uploader.onProgressItem = function (fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
      };
      uploader.onProgressAll = function (progress) {
        console.info('onProgressAll', progress);
      };
      uploader.onSuccessItem = function (fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
      };
      uploader.onErrorItem = function (fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
      };
      uploader.onCancelItem = function (fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
      };
      uploader.onCompleteItem = function (fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
      };
      uploader.onCompleteAll = function () {
        console.info('onCompleteAll');
      };

      console.info('uploader', uploader);
    }
    

}]);
