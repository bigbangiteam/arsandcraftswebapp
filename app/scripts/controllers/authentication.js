﻿'use strict';

angular.module('arsandcraftsApp')

.controller('LoginController',
    ['$scope', '$rootScope', '$location', 'AuthenticationService',
    function ($scope, $rootScope, $location, AuthenticationService) {
		if($rootScope.globals != null) {		
			$rootScope.loginoutString='Login';
			$rootScope.isLogged=false;
		} else {		
			$rootScope.loginoutString='Logout';
			$rootScope.isLogged=true;
			// reset login status
			AuthenticationService.ClearCredentials();
			location.path('/home');
		 }
		$scope.error='Credenziali non valide!';
         $scope.errorShow=false;

        $scope.login = function () {
		  AuthenticationService.ClearCredentials();
          $scope.errorShow=false;
          $scope.dataLoading = true;
          AuthenticationService.SetCredentials($scope.username, $scope.password);

          AuthenticationService.Login(function (response) {
				console.log("respone:", response);
              if (response.status==200 && response.data.status==0) {
                $scope.errorShow=false;				
				$rootScope.loginoutString='Logout';
				$rootScope.isLogged=true;
                AuthenticationService.loginOK($scope.username, $scope.password);
                $location.path('/artigiano');

              } else {
				$rootScope.isLogged=false;
				$scope.errorShow=true;
                $scope.error = "Credenziali non valide. Riprovare.";
                AuthenticationService.ClearCredentials();
                $scope.dataLoading = false;
              }
          });
        };
    }]);
