/**
 * Created by daniele on 26/07/15.
 */

angular.module('arsandcraftsApp')
  .service('ArtigianoSrvc',
  ['$http', '$timeout', 'BaseSrvc',     function ($http, $timeout, BaseSrvc) {


    this.getArtigiano = function(artigianoId){
      return $http({
        url: BaseSrvc.restURLS.URLgetArtigano + '/' + artigianoId,
        method: "GET"
      });
    };

    this.getAllProdotto = function(){
      return $http({
        url: BaseSrvc.restURLS.URLgetAllProdotto,
        method: "GET"
      });
    };

    this.getProdottoById = function(prodottoId){
      return $http({
        url: BaseSrvc.restURLS.URLgetProdottoById + '/' + prodottoId,
        method: "GET"
      });
    };
    
    this.deleteProdottoById = function(prodottoId){
      return $http({
        url: BaseSrvc.restURLS.URLdeleteProdottoById + '/' + prodottoId,
        method: "DELETE"
      });
    };

    this.getDataImmagine = function(immagineId){
      return BaseSrvc.restURLS.URLgetDataImmagineById + '/' + immagineId;
    };

  }]);
