/**
 * Created by daniele on 26/07/15.
 */
angular.module('arsandcraftsApp')
  .service('BaseSrvc',
  [function () {

    var base = '/arsandcrafts';

    this.restURLS = {
      URLgetArtigano:         base + '/rest/public/utente/artigiano',
      URLgetAllProdotto:      base + '/rest/artisan/prodotto',
      URLgetProdottoById:     base + '/rest/public/prodotto',
      URLgetDataImmagineById: base + '/rest/public/file/foto',
      URLdeleteProdottoById:  base +  '/rest/artisan/foto',
      URLUploadImages:        base + '/rest/public/file/foto'
    };
  }]);
