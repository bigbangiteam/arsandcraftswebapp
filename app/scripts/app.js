'use strict';

/**
 * @ngdoc overview
 * @name arsandcraftsApp
 * @description
 * # arsandcraftsApp
 *
 * Main module of the application.
 */
angular
  .module('arsandcraftsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'angularFileUpload',
    'ui.bootstrap'
  ])
  .config(['$routeProvider', function ($routeProvider) {


    $routeProvider
      .when('/login', {
        controller: 'LoginController',
        templateUrl: 'views/login.html'
      })
      .when('/home', {
        controller: 'HomeController',
        templateUrl: 'views/home.html'
      })
      .when('/artigiano', {
        controller: 'ArtigianoController',
        templateUrl: 'views/artigiano.html'
      })

      .otherwise({redirectTo: '/home'});
  }])
  .run(['$rootScope', '$location', '$cookieStore', '$http',
    function ($rootScope, $location, $cookieStore, $http) {
		
		

      //$http.defaults.headers.common = {"Access-Control-Request-Headers": "accept, origin, authorization"};

      $rootScope.globals = {};
	  $rootScope.loginoutString='Login';
	  $rootScope.isLogged=false;
		
      // keep user logged in after page refresh
      $rootScope.globals = $cookieStore.get('globals') || {};
      if ($rootScope.globals.currentUser) {	  
		$rootScope.loginoutString='Logout';
		$rootScope.isLogged=true;
        $http.defaults.headers.common.Authorization = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
      }

      $rootScope.$on('$locationChangeStart', function (event, next, current) {
        // redirect to login page if not logged in
        if ($location.path() !== '/home' && (!$location.path() === '/login') && !$rootScope.globals.currentUser) {
          $location.path('/home');
        }

        $rootScope.showCarousel = ($location.path() === '/home');
        
        $rootScope.gotoLogin = function() {
			$location.path('/login');
		};
		$rootScope.gotoArtigiano = function() {
			$location.path('/artigiano');
		};
		
      });
    }
  ]
);
